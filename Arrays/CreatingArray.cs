﻿using System;

namespace WorkingWithArrays
{
    public static class CreatingArray
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Performance", "CA1825:Avoid zero-length array allocations", Justification = "Education purposes")]
        public static int[] CreateEmptyArrayOfIntegers()
        {
            return new int[0];
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Performance", "CA1825:Avoid zero-length array allocations", Justification = "Education purposes")]
        public static bool[] CreateEmptyArrayOfBooleans()
        {
            bool[] array = { };
            return array;
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Performance", "CA1825:Avoid zero-length array allocations", Justification = "Education purposes")]
        public static string[] CreateEmptyArrayOfStrings()
        {
            return new string[] { };
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Performance", "CA1825:Avoid zero-length array allocations", Justification = "Education purposes")]
        public static char[] CreateEmptyArrayOfCharacters()
        {
            return new char[] { };
        }

        public static double[] CreateEmptyArrayOfDoubles()
        {
            return Array.Empty<double>();
        }

        public static float[] CreateEmptyArrayOfFloats()
        {
            return Array.Empty<float>();
        }

        public static decimal[] CreateEmptyArrayOfDecimals()
        {
            return Array.Empty<decimal>();
        }

        public static int[] CreateArrayOfTenIntegersWithDefaultValues()
        {
            return new int[10];
        }

        public static bool[] CreateArrayOfTwentyBooleansWithDefaultValues()
        {
            return new bool[20];
        }

        public static string[] CreateArrayOfFiveEmptyStrings()
        {
            return new string[5];
        }

        public static char[] CreateArrayOfFifteenCharactersWithDefaultValues()
        {
            return new char[15];
        }

        public static double[] CreateArrayOfEighteenDoublesWithDefaultValues()
        {
            return new double[18];
        }

        public static float[] CreateArrayOfOneHundredFloatsWithDefaultValues()
        {
            return new float[100];
        }

        public static decimal[] CreateArrayOfOneThousandDecimalsWithDefaultValues()
        {
            return new decimal[1000];
        }

        public static int[] CreateIntArrayWithOneElement()
        {
            int[] array = { 123456 };
            return array;
        }

        public static int[] CreateIntArrayWithTwoElements()
        {
            int[] array = { 1111111, 9999999 };
            return array;
        }

        public static int[] CreateIntArrayWithTenElements()
        {
            int[] array = { 0, 4234, 3845, 2942, 1104, 9794, 0923943, 7537, 4162, 10134 };
            return array;
        }

        public static bool[] CreateBoolArrayWithOneElement()
        {
            bool[] array = { true };
            return array;
        }

        public static bool[] CreateBoolArrayWithFiveElements()
        {
            bool[] array = { true, false, true, false, true };
            return array;
        }

        public static bool[] CreateBoolArrayWithSevenElements()
        {
            bool[] array = { false, true, true, false, true, true, false };
            return array;
        }

        public static string[] CreateStringArrayWithOneElement()
        {
            string[] array = { "one" };
            return array;
        }

        public static string[] CreateStringArrayWithThreeElements()
        {
            string[] array = { "one", "two", "three" };
            return array;
        }

        public static string[] CreateStringArrayWithSixElements()
        {
            string[] array = { "one", "two", "three", "four", "five", "six" };
            return array;
        }

        public static char[] CreateCharArrayWithOneElement()
        {
            char[] array = { 'a' };
            return array;
        }

        public static char[] CreateCharArrayWithThreeElements()
        {
            char[] array = { 'a', 'b', 'c' };
            return array;
        }

        public static char[] CreateCharArrayWithNineElements()
        {
            char[] array = { 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'a' };
            return array;
        }

        public static double[] CreateDoubleArrayWithOneElement()
        {
            double[] array = { 1.12 };
            return array;
        }

        public static double[] CreateDoubleWithFiveElements()
        {
            double[] array = { 1.12, 2.23, 3.34, 4.45, 5.56 };
            return array;
        }

        public static double[] CreateDoubleWithNineElements()
        {
            double[] array = { 1.12, 2.23, 3.34, 4.45, 5.56, 6.67, 7.78, 8.89, 9.91 };
            return array;
        }

        public static float[] CreateFloatArrayWithOneElement()
        {
            float[] array = { 123456789.123456f };
            return array;
        }

        public static float[] CreateFloatWithThreeElements()
        {
            float[] array = { 1000000.123456f, 2223334444.123456f, 9999.999999f };
            return array;
        }

        public static float[] CreateFloatWithFiveElements()
        {
            float[] array = { 1.0123f, 20.012345f, 300.01234567f, 4000.01234567f, 500000.01234567f };
            return array;
        }

        public static decimal[] CreateDecimalArrayWithOneElement()
        {
            decimal[] array = { 10000.123456m };
            return array;
        }

        public static decimal[] CreateDecimalWithFiveElements()
        {
            decimal[] array = { 1000.1234m, 100000.2345m, 100000.3456m, 1000000.456789m, 10000000.5678901m };
            return array;
        }

        public static decimal[] CreateDecimalWithNineElements()
        {
            decimal[] array = { 10.122112M, 200.233223M, 3000.344334M, 40000.455445M, 500000.566556M, 6000000.677667M, 70000000.788778M, 800000000.899889M, 9000000_000.911991M };
            return array;
        }
    }
}
